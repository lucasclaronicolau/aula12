import React from 'react';
import Investimento from "./Investimento";
import ValorTotal from "./ValorTotal";
import Meses from "./Meses";

export default class Calculadora extends React.Component{
    
    constructor(props){
        super(props)

        this.state = {
            investimento : 1000,
            valorTotal : 12000,
            meses : 12
        }
    }
    
    changeValuesI = (e) => {
        this.setState({
            investimento : e,
            meses: this.state.valorTotal/this.state.investimento,
            valorTotal: this.state.meses*this.state.investimento
        })
      }

    changeValuesM = (e) => {
        this.setState({
            meses: e,
            investimento: this.state.valorTotal/this.state.meses,
            valorTotal: this.state.meses*this.state.investimento
        })
    }
    
    changeValuesV = (e) => {
        this.setState({
            valorTotal : e,
            investimento: this.state.valorTotal/this.state.meses,
            meses: this.state.valorTotal/this.state.investime
        })
    }

    recalculo = () => {
        this.setState({
            
        })
    }
      
    render(){
        return (
            <div>
                <Investimento value= {this.state.investimento} change={this.changeValuesI}></Investimento>
                <br/>
                <Meses value = {this.state.meses} change={this.changeValuesM}></Meses>
                <br/>
                <ValorTotal value = {this.state.valorTotal} change={this.changeValuesV}></ValorTotal>
            </div>
        )
    }
}