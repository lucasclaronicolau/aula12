import React from 'react'

export default class Investimento extends React.Component{
    
    onChange = (e) => {
        this.props.change(e.target.value);
    }
    
    render(){
        return(
            <div>
                <p>Investimento:</p>
                <input name="investimento" type="number" onChange = {this.onChange} value={this.props.value}></input>
            </div>
        )
    }
}