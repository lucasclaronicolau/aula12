import React from 'react'

export default class Meses extends React.Component{
    
    onChange = (e) => {
        this.props.change(e.target.value);
    }

    render(){
        return (
            <div>
                <p>Meses:</p>
                <input name="meses" type="number" onChange = {this.onChange} value={this.props.value}></input>
            </div>
        )
    }
}