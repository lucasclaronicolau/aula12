import React from 'react'

export default class ValorTotal extends React.Component{

    onChange = (e) => {
        this.props.change(e.target.value);
    }    
    
    render(){
        return(
            <div>
                <p>Valor Total:</p>
                <input name="valorTotal" type="number" onChange = {this.onChange} value={this.props.value}></input>
            </div>
        )
    }
}